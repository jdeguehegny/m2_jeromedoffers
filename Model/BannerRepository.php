<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model;

use Magento\Framework\Api\SearchResultsFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\Stdlib\DateTime\DateTime;
use JeromeD\Offers\Api\BannerRepositoryInterface;
use JeromeD\Offers\Model\ResourceModel\Banner\CollectionFactory as BannerCollectionFactory;
use JeromeD\Offers\Model\ResourceModel\Banner\Collection;
use JeromeD\Offers\Model\ResourceModel\Banner as ResourceModel;

/**
 * Class BannerRepository
 */
class BannerRepository
{
    /**
     * @var BannerFactory
     */
    private $bannerFactory;

    /**
     * @var BannerCollectionFactory
     */
    private $bannerCollectionFactory;

    /**
     * @var SearchResultsFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessor
     **/
    private $collectionProcessor;

    /**
     * @var ResourceModel
     */
    private $resourceModel;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * BannerRepository constructor.
     *
     * @param BannerFactory              $bannerFactory
     * @param BannerCollectionFactory    $bannerCollectionFactory
     * @param SearchResultsFactory     $searchResultFactory
     * @param CollectionProcessor      $collectionProcessor
     * @param ResourceModel            $resourceModel
     */
    public function __construct(
        DateTime $date,
        BannerFactory $bannerFactory,
        BannerCollectionFactory $bannerCollectionFactory,
        SearchResultsFactory $searchResultFactory,
        CollectionProcessor $collectionProcessor,
        ResourceModel $resourceModel
    ) {
        $this->date = $date;
        $this->bannerFactory = $bannerFactory;
        $this->bannerCollectionFactory = $bannerCollectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->resourceModel = $resourceModel;
    }

    /**
     * Get Banner by its ID
     *
     * @param $bannerId
     * @return Banner
     */
    public function getById($bannerId)
    {
        $banner = $this->bannerFactory->create();

        $banner->load($bannerId);
        if (!$banner->getId()) {
            throw new NoSuchEntityException(
                __("The banner that was requested doesn't exist. Verify the banner and try again.")
            );
        }

        return $banner;
    }

    /**
     * Get active banner collection by category id
     *
     * @param null $id
     * @return Collection
     */
    public function getActiveByCategoryCollection($id = null) {
        $collection = $this->bannerFactory->create()->getCollection();

        $collection->join(
            ['banner_category' => $collection->getTable('jeromed_banner_category')],
            'main_table.banner_id=banner_category.banner_id AND banner_category.category_id = ' . $id,
            []
        );

        $collection->getSelect()
            ->where('display_start is null OR display_start <= ?', $this->date->date())
            ->where('display_end is null OR display_end >= ?', $this->date->date());

        return $collection;
    }
}
