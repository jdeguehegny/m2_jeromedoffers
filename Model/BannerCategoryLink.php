<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model;

/**
 * Class BannerCategoryLink
 */
class BannerCategoryLink extends \Magento\Framework\Api\AbstractSimpleObject
{
    const BANNER_ID = 'banner_id';
    const CATEGORY_ID = 'category_id';

    /**
     * @return mixed|null
     */
    public function getBannerId()
    {
        return $this->_get(self::BANNER_ID);
    }

    /**
     * @return mixed|null
     */
    public function getCategoryId()
    {
        return $this->_get(self::CATEGORY_ID);
    }

    /**
     * @param $bannerId
     * @return BannerCategoryLink
     */
    public function setBannerId($bannerId)
    {
        return $this->setData(self::BANNER_ID, $bannerId);
    }

    /**
     * @param $categoryId
     * @return BannerCategoryLink
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }
}
