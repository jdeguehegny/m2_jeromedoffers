<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model\Banner;

use JeromeD\Offers\Helper\Image;
use JeromeD\Offers\Model\ResourceModel\Banner\CollectionFactory;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var Image
     */
    protected $imageHelper;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $bannerCollectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $bannerCollectionFactory,
        Image $imageHelper,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $bannerCollectionFactory->create();
        $this->imageHelper = $imageHelper;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data from collection
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        $this->loadedData = array();

        foreach ($items as $banner) {
            $banner->loadCategoryIds();
            $tmpData = $banner->getData();

            if($tmpData['image']) {
                $image = [
                    0 => [
                        'image' => $tmpData['image'],
                        'url' => $this->imageHelper->getImageUrl($tmpData['image']),
                    ],
                ];
                $tmpData['image'] = $image;
            }

            $this->loadedData[$banner->getBannerId()]['banner'] = $tmpData;
        }

        return $this->loadedData;
    }
}
