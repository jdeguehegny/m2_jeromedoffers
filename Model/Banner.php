<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Banner
 */
class Banner extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'jeromed_banner';

    /**
     * @var string
     */
    protected $cacheTag = 'jeromed_banner';

    /**
     * @var string
     */
    protected $eventPrefix = 'jeromed_banner';

    /**
     * @var
     */
    protected $categoryIds;

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('JeromeD\Offers\Model\ResourceModel\Banner');
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }

    /**
     * Load and set banner Categories
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadCategoryIds()
    {
        if (!$this->categoryIds) {
            $ids = $this->_getResource()->getCategoryIds($this);
            $this->setCategoryIds($ids);
        }
    }

    /**
     * @return mixed
     */
    public function getCategoryIds()
    {
        return $this->categoryIds;
    }

    /**
     * @param mixed $categoryIds
     */
    public function setCategoryIds($categoryIds)
    {
        $this->categoryIds = $categoryIds;
        $this->setData('categoryIds', $categoryIds);
    }
}
