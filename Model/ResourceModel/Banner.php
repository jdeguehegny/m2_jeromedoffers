<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model\ResourceModel;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\App\ObjectManager;
use JeromeD\Offers\Model\ResourceModel\Banner\Category\Link;

/**
 * Class Banner
 */
class Banner extends AbstractDb
{
    /**
     * Banner constructor.
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('jeromed_banner', 'banner_id');
    }

    /**
     * Save data related with banner
     *
     * @param AbstractModel $banner
     * @return $this
     */
    protected function _afterSave(AbstractModel $banner)
    {
        $this->_saveCategoryIds($banner);
        return parent::_afterSave($banner);
    }

    /**
     * @param $banner
     * @return $this
     */
    protected function _saveCategoryIds($banner)
    {
        if (count($banner->getData('categoryIds'))) {
            $categoryIds = $banner->getData('categoryIds');
            $this->getBannerCategoryLink()->saveCategoryIds($banner, $categoryIds);
        }

        return $this;
    }

    /**
     * @param $banner
     * @return mixed
     */
    public function getCategoryIds($banner)
    {
        return $this->getBannerCategoryLink()->getCategoryIdsByBannerId($banner->getId());
    }

    /**
     * @return mixed
     */
    private function getBannerCategoryLink()
    {
        return ObjectManager::getInstance()->get(Link::class);
    }
}
