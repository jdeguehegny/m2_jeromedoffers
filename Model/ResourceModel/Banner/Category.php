<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model\ResourceModel\Banner;

/**
 * Class Category
 */
class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize connection and define resource table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('jeromed_banner_category', 'banner_id');
    }

    /**
     * @param $bannerId
     * @return Category
     * @throws \Exception
     */
    public function removeAllCategories($bannerId)
    {
        return $this->removeCategories($bannerId, [], true);
    }

    /**
     * @param $bannerId
     * @param $categoryIds
     * @param bool $all
     * @return $this
     * @throws \Exception
     */
    public function removeCategories($bannerId, $categoryIds, $all = false)
    {
        if (!$bannerId || (!$all && (!is_array($categoryIds) || count($categoryIds) == 0))) {
            return $this;
        }

        $connection = $this->getConnection();
        $whereCond = [
            $connection->quoteInto('banner_id = ?', $bannerId)
        ];

        if (!$all) {
            $connection->quoteInto('category_id IN(?)', $categoryIds);
        }

        $whereCond = join(' AND ', $whereCond);

        $connection->beginTransaction();
        try {
            $connection->delete($this->getMainTable(), $whereCond);
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

        return $this;
    }

    /**
     * @param $bannerId
     * @param $categoryIds
     * @return $this
     * @throws \Exception
     */
    public function addCategories($bannerId, $categoryIds)
    {
        if (!$bannerId || !is_array($categoryIds) || count($categoryIds) == 0) {
            return $this;
        }

        $this->getConnection()->beginTransaction();

        // Before adding of products we should remove it old rows with same ids
        $this->removeCategories($bannerId, $categoryIds);
        try {
            foreach ($categoryIds as $categoryId) {
                $this->getConnection()->insert(
                    $this->getMainTable(),
                    ['banner_id' => (int)$bannerId, 'category_id' => (int)$categoryId]
                );
            }
            $this->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getConnection()->rollBack();
            throw $e;
        }
        return $this;
    }

    /**
     * @param $bannerId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategories($bannerId)
    {
        $select = $this->getConnection()->select()->from(
            $this->getMainTable(),
            ['banner_id', 'category_id']
        )->where(
            'banner_id IN (?)',
            $bannerId
        );
        $rowset = $this->getConnection()->fetchAll($select);

        $result = [];
        foreach ($rowset as $row) {
            $result[$row['banner_id']][] = $row['category_id'];
        }

        return $result;
    }
}
