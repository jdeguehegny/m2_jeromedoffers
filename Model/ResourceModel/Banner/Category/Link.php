<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Model\ResourceModel\Banner\Category;

use Magento\Framework\App\ResourceConnection;
use JeromeD\Offers\Model\Banner;

/**
 * Class Link
 */
class Link
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resourceConnection;

    /**
     * Link constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param $bannerId
     * @return array
     */
    public function getCategoryIdsByBannerId($bannerId)
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()->from(
            $this->getBannerCategoryTable(),
            'category_id'
        )->where(
            'banner_id = ?',
            (int) $bannerId
        );

        return $connection->fetchCol($select);
    }

    /**
     * @param Banner $banner
     * @param array $categoryIds
     * @return bool
     */
    public function saveCategoryIds(Banner $banner, array $categoryIds)
    {
        $connection = $this->resourceConnection->getConnection();

        $oldCategoryIds = $this->getCategoryIdsByBannerId($banner->getId());
        $insert = array_diff($categoryIds, $oldCategoryIds);
        $delete = array_diff($oldCategoryIds, $categoryIds);

        if (!empty($insert)) {
            $data = [];
            foreach ($insert as $categoryId) {
                $data[] = ['banner_id' => (int) $banner->getId(), 'category_id' => (int) $categoryId];
            }
            $connection->insertMultiple($this->getBannerCategoryTable(), $data);
        }

        if (!empty($delete)) {
            foreach ($delete as $categoryId) {
                $condition = ['banner_id = ?' => (int) $banner->getId(), 'category_id = ?' => (int) $categoryId];
                $connection->delete($this->getBannerCategoryTable(), $condition);
            }
        }

        if (!empty($insert) || !empty($delete)) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    private function getBannerCategoryTable()
    {
        return $this->resourceConnection->getTableName('jeromed_banner_category');
    }
}
