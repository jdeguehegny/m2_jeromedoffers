<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Controller\Adminhtml;

use JeromeD\Offers\Model\BannerRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use JeromeD\Offers\Model\BannerFactory;
use Magento\Framework\Registry;

/**
 * Class Banner
 */
abstract class Banner extends Action
{
    const ADMIN_RESOURCE = 'JeromeD_Offers::banner';
    const CURRENT_BANNER_ID = 'current_jeromed_banner_id';

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $coreRegistry;

    /**
     * Banner Factory
     *
     * @var BannerFactory
     */
    protected $bannerFactory;

    /**
     * Banner Repository
     *
     * @var BannerRepository
     */
    protected $bannerRepository;

    /**
     * constructor
     *
     * @param Registry $coreRegistry
     * @param BannerFactory $bannerFactory
     * @param BannerRepository $bannerRepository
     * @param Context $context
     */
    public function __construct(
        Registry $coreRegistry,
        BannerFactory $bannerFactory,
        BannerRepository $bannerRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        Context $context
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->bannerFactory = $bannerFactory;
        $this->bannerRepository = $bannerRepository;
        $this->resultForwardFactory = $resultForwardFactory;

        parent::__construct($context);
    }

    /**
     * Init Banner
     *
     * @return \JeromeD\Offers\Model\Banner
     */
    protected function initBanner()
    {
        $bannerId = (int) $this->getRequest()->getParam('banner_id');

        if ($bannerId) {
            $this->coreRegistry->register(self::CURRENT_BANNER_ID, $bannerId);
        }

        /** @var \JeromeD\Offers\Model\Banner $banner */
        $banner = $this->bannerFactory->create();
        if ($bannerId) {
            $banner = $this->bannerRepository->getById($bannerId);
        }

        return $banner;
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE);
    }
}
