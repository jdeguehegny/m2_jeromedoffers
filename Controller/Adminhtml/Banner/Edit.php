<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Controller\Adminhtml\Banner;

use JeromeD\Offers\Model\BannerRepository;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use JeromeD\Offers\Controller\Adminhtml\Banner;
use JeromeD\Offers\Model\BannerFactory;

/**
 * Class Edit
 */
class Edit extends Banner
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ImageUploader
     */
    protected $imageUploader;

    /**
     * Edit constructor.
     *
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param BannerFactory $bannerFactory
     * @param BannerRepository $bannerRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        BannerFactory $bannerFactory,
        BannerRepository $bannerRepository,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        Context $context
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($coreRegistry, $bannerFactory, $bannerRepository, $resultForwardFactory, $context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|ResponseInterface|Redirect|ResultInterface|Page
     */
    public function execute()
    {
        /** @var \JeromeD\Offers\Model\Banner $banner */
        $banner = $this->initBanner();

        /** @var \Magento\Backend\Model\View\Result\Page|Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('JeromeD_Offers::banner');
        $resultPage->getConfig()->getTitle()
            ->prepend($banner->getId() ? __('Edit Banner') : __('New Banner'));

        return $resultPage;
    }
}
