<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Controller\Adminhtml\Banner;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use JeromeD\Offers\Model\BannerRepository;
use JeromeD\Offers\Model\ImageManager;
use JeromeD\Offers\Controller\Adminhtml\Banner;
use JeromeD\Offers\Model\BannerFactory;

/**
 * Class Save
 */
class Save extends Banner implements HttpPostActionInterface
{
    /**
     * Page factory
     *
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\Filter\Date
     */
    protected $dateFilter;

    /**
     * Edit constructor.
     *
     * @param ImageManager $imageManager
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param BannerFactory $bannerFactory
     * @param BannerRepository $bannerRepository
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param Context $context
     */
    public function __construct(
        ImageManager $imageManager,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        BannerFactory $bannerFactory,
        BannerRepository $bannerRepository,
        \Magento\Framework\Stdlib\DateTime\Filter\Date $dateFilter,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        Context $context
    ) {
        $this->imageManager = $imageManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->dateFilter = $dateFilter;

        parent::__construct($coreRegistry, $bannerFactory, $bannerRepository, $resultForwardFactory, $context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page|ResponseInterface|Redirect|ResultInterface|Page
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if ($this->getRequest()->getPostValue()) {
            $bannerDatas = $this->getRequest()->getParam('banner');

            $resultRedirect = $this->resultRedirectFactory->create();

            if (is_array($bannerDatas)) {
                $bannerDatas = $this->filterValues($bannerDatas);

                if (isset($bannerDatas['image']) && isset($bannerDatas['image'][0]) && is_array($bannerDatas['image'][0])) {
                    if (isset($bannerDatas['image'][0]['tmp_name'])) {
                        $filename = $bannerDatas['image'][0]['name'];
                        $this->imageManager->moveFileFromTmp($filename);
                    } else {
                        $filename = $bannerDatas['image'][0]['image'];
                    }
                    $bannerDatas['image'] = $filename;
                }
                $banner = $this->_objectManager->create(\JeromeD\Offers\Model\Banner::class);
                $banner->setData($bannerDatas)->save();
            }
        }

        return $resultRedirect->setPath('*/*/index');
    }

    /**
     * Filter/format data values
     *
     * @param $datas
     * @return mixed
     */
    protected function filterValues($datas) {
        $filterValues = ['display_start' => $this->dateFilter];
        $filterValues['display_end'] = $this->dateFilter;
        $inputFilter = new \Zend_Filter_Input(
            $filterValues,
            [],
            $datas
        );
        return $inputFilter->getUnescaped();
    }
}
