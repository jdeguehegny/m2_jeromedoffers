/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

var config = {
    paths: {
        'jeromed/offers/slick': 'JeromeD_Offers/js/slick.min'
    },
    shim: {
        "jeromed/offers/slick": ["jquery"]
    }
};
