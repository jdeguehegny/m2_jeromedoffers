<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Block\Banner;

use JeromeD\Offers\Helper\Data;
use JeromeD\Offers\Helper\Image;
use JeromeD\Offers\Model\ResourceModel\Banner\Collection;

/**
 * Class View
 */
class View extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'jeromed_banner';

    /**
     * @var Collection
     */
    protected $bannerCollection;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Catalog\Helper\Category
     */
    protected $_categoryHelper;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var Image
     */
    protected $helperImage;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Data $helperData
     * @param Image $helperImage
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        Data $helperData,
        Image $helperImage,
        \Magento\Catalog\Helper\Category $categoryHelper,
        array $data = []
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->_coreRegistry = $registry;
        $this->helperData = $helperData;
        $this->helperImage = $helperImage;

        parent::__construct($context, $data);

        $this->addData([
            'cache_lifetime' => 3600,
        ]);
    }

    /**
     * @return Image
     */
    public function getHelperImage() {
        return $this->helperImage;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        $category = $this->getCurrentCategory();

        $this->bannerCollection = null;
        if ($category) {
            $this->bannerCollection = $this->helperData->getBannerCategoryCollection($category->getId());
        }

        return parent::_prepareLayout();
    }

    /**
     * @return mixed
     */
    public function getBannerCollection()
    {
        return $this->bannerCollection;
    }

    /**
     * Retrieve current category model object
     *
     * @return \Magento\Catalog\Model\Category
     */
    public function getCurrentCategory()
    {
        if (!$this->hasData('current_category')) {
            $this->setData('current_category', $this->_coreRegistry->registry('current_category'));
        }
        return $this->getData('current_category');
    }

    /**
     * Get cache key informative items
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array_merge(
            parent::getCacheKeyInfo(),
            ['CATEGORY_ID' => $this->getCurrentCategory()->getId()]
        );
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [
            self::CACHE_TAG . '_' . $this->getCurrentCategory()->getId(),
        ];

        return array_unique($identities);
    }
}
