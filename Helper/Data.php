<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Stdlib\DateTime\DateTime;
use JeromeD\Offers\Model\BannerRepository;
use JeromeD\Offers\Model\ResourceModel\Banner\Collection;

/**
 * Class Data
 */
class Data
{
    /**
     * @var Context
     */
    public $context;

    /**
     * @var BannerRepository
     */
    public $bannerRepository;

     /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * Data constructor.
     *
     * @param DateTime $date
     * @param Context $context
     * @param HttpContext $httpContext
     * @param BannerRepository $bannerRepository
     */
    public function __construct(
        DateTime $date,
        Context $context,
        HttpContext $httpContext,
        BannerRepository $bannerRepository
    ) {
        $this->date          = $date;
        $this->context       = $context;
        $this->httpContext   = $httpContext;
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Get Banner Collection by Category
     *
     * @param null $category_id
     *
     * @return Collection
     */
    public function getBannerCategoryCollection($category_id = null, $limit = 5)
    {
        $collection = $this->bannerRepository->getActiveByCategoryCollection($category_id);

        $collection->addOrder('display_end', 'ASC');
        $collection->setPageSize($limit);

        return $collection;
    }
}
