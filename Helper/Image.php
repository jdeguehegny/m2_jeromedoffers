<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Helper;

use Magento\Framework\UrlInterface;
use Magento\Framework\Filesystem;

/**
 * Class Image
 */
class Image
{
    /**
     * media folder
     * @var string
     */
    const MEDIA_DIR = 'jeromedoffers/banner';

    /**
     * media tmp folder
     * @var string
     */
    const MEDIA_TMP_DIR = 'jeromedoffers/tmp/banner';

    /**
     * url builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $fileSystem;

    /**
     * Constructor
     *
     * @param UrlInterface $urlBuilder
     * @param Filesystem $fileSystem
     */
    public function __construct(
        UrlInterface $urlBuilder,
        Filesystem $fileSystem
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->fileSystem = $fileSystem;
    }

    /**
     * Get image base Url
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]). self::MEDIA_DIR . '/';
    }

    /**
     * Get image full Url
     *
     * @param $name
     *
     * @return string
     */
    public function getImageUrl($name)
    {
        return $this->getBaseUrl() . $name;
    }
}
