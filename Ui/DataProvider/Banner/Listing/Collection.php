<?php

namespace JeromeD\Offers\Ui\DataProvider\Banner\Listing;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

class Collection extends SearchResult
{
    /**
     * Override _initSelect to add custom columns
     *
     * @return void
     */
    protected function _initSelect()
    {
        $this->addFilterToMap('banner_id', 'main_table.banner_id');
        parent::_initSelect();
    }
}
