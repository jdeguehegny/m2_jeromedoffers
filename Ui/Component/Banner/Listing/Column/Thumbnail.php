<?php
/**
 * JeromeD
 *
 * @category    JeromeD
 * @package     JeromeD_Offers
 * @license     http://opensource.org/licenses/gpl-license.php GNU Public License
 * @author      Jérôme Deguehegny <j.deguehegny@gmail.com>
 */

namespace JeromeD\Offers\Ui\Component\Banner\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use JeromeD\Offers\Helper\Image;

class Thumbnail extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Constructor
     *
    * @param ContextInterface $context
    * @param UiComponentFactory $uiComponentFactory
    * @param Image $imageHelper
    * @param array $components
    * @param array $data
    */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Image $imageHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);

        $this->imageHelper = $imageHelper;
    }

    /**
    * Prepare Data Source
    *
    * @param array $dataSource
    * @return array
    */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = "image";
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['image'])) {
                    $filename = $item['image'];
                    $item[$fieldName . '_src'] = $this->imageHelper->getBaseUrl() . $filename;
                    $item[$fieldName . '_alt'] = $filename;
                    $item[$fieldName . '_orig_src'] = $this->imageHelper->getBaseUrl() . $filename;
                }
            }
        }

        return $dataSource;
    }
}
